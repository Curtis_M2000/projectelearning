﻿using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjetELearning.Models;

namespace ProjetELearning.Controllers
{
    public class StudentsController : Controller
    {
        public static string Id = "";

        public ActionResult setId(string id)
        {
            Id = id;
            return RedirectToAction("Profile", "Students");
        }

        public ActionResult Profile()
        {
            if(Id == "")
            {
                return RedirectToAction("Index", "Home");
            }

            else
            {
                studentsDataContext db = new studentsDataContext();
                var fetch = from x in db.students where x.Id == Id select x;
                foreach (var st in fetch)
                {
                    ViewBag.id = st.Id;
                    ViewBag.name = st.name;
                    ViewBag.email = st.email;
                    ViewBag.tel = st.tel;
                    ViewBag.grade = st.grade;
                    ViewBag.picture = st.picture;
                    ViewBag.firstconn = st.firstconn;
                }
                return View();
            } 
        }

        [HttpPost]
        public new ActionResult Profile(student formpost)
        {
            studentsDataContext db = new studentsDataContext();

            var studentlist = from x in db.students where x.Id == Id select x;

            foreach (student st in studentlist)
            {
                st.name = formpost.name;
                st.password = formpost.password;
                st.firstconn = 1;
            }

            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return RedirectToAction("Profile", "Students");
        }
        public ActionResult All_courses()
        {
            if (Id == "")
            {
                return RedirectToAction("Index", "Home");
            }

            else
            {
                List<int> coursid = new List<int>();

                studentsDataContext db = new studentsDataContext();
                var courses = from x in db.courses select x;
                var studentcourse = from x in db.studentcourses where x.studentid == Id select x;
                var student = from x in db.students where x.Id == Id select x;

                foreach(var st in student)
                {
                    ViewBag.level = st.grade;
                }

                foreach(var cr in studentcourse)
                {
                    coursid.Add(cr.coursid);
                }
                ViewBag.courselist = coursid;
                
                return View(courses);
            }              
        }

        public ActionResult Subscription(int id)
        {
            studentsDataContext db = new studentsDataContext();

            studentcourse subscribe = new studentcourse();
            subscribe.studentid = Id;
            subscribe.coursid = id;

            db.studentcourses.InsertOnSubmit(subscribe);
            db.SubmitChanges();

            return RedirectToAction("All_courses", "Students");
        }

        public ActionResult My_course()
        {
            if (Id == "")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                studentsDataContext db = new studentsDataContext();
                var courses = from x in db.courses from y in db.studentcourses where x.Id == y.coursid && y.studentid == Id select x;
                return View(courses);
            }
            
        }

        public ActionResult Forum()
        {
            if (Id == "")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                studentsDataContext db = new studentsDataContext();
                var forum = from x in db.forums from y in db.students where x.personId == y.Id select new myforum { name = y.name, picture = y.picture, date = x.commentDate, message = x.message };
                return View(forum);
            }              
        }

        [HttpPost]
        public ActionResult Forum(forum forumPost)
        {
            studentsDataContext db = new studentsDataContext();

            forumPost.personId = Id;
            forumPost.commentDate = DateTime.Now;

            db.forums.InsertOnSubmit(forumPost);
            db.SubmitChanges();
            return RedirectToAction("forum", "Students");
        }

        public ActionResult Course(int id)
        {
            if (Id == "")
            {
                return RedirectToAction("Index", "Home");
            }

            else
            {
                studentsDataContext db = new studentsDataContext();
                var courses = from x in db.courses where x.Id == id select x;

                foreach(var fetch in courses)
                {
                    ViewBag.name = fetch.nom;
                    ViewBag.level = fetch.level;
                    ViewBag.desc = fetch.desc;
                    ViewBag.image = fetch.image;
                    ViewBag.video = fetch.video;
                    ViewBag.notes = fetch.notes;
                    ViewBag.exo = fetch.exo;
                    ViewBag.solution = fetch.solution;
                }

                return View();
            }
        }

        public ActionResult Research(string id)
        {
            if (Id == "")
            {
                return RedirectToAction("Index", "Home");
            }

            else
            {
                List<int> coursid = new List<int>();

                studentsDataContext db = new studentsDataContext();
                var courses = from x in db.courses where SqlMethods.Like(x.nom, "%" + id + "%") || SqlMethods.Like(x.level, "%" + id + "%") || SqlMethods.Like(x.desc, "%" + id + "%") select x;
                var studentcourse = from x in db.studentcourses where x.studentid == Id select x;
                var student = from x in db.students where x.Id == Id select x;

                foreach (var st in student)
                {
                    ViewBag.level = st.grade;
                }

                foreach (var cr in studentcourse)
                {
                    coursid.Add(cr.coursid);
                }
                ViewBag.courselist = coursid;

                return View(courses);
            }    
        }

        public ActionResult Logout()
        {
            Id = "";
            return RedirectToAction("Index", "Home");
        }

        public ActionResult SelectedCourse(int id)
        {
            studentsDataContext db = new studentsDataContext();
            var course = from x in db.courses where x.Id == id select x;
            return View(course);
        }
    }
}