﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using ProjetELearning.Models;

namespace ProjetELearning.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            student user = new student();
            return View(user);
        }

        [HttpPost]
        public ActionResult Index(student user)
        {
            string email = user.email;
            string tel = user.tel;
            string password = user.password;

            studentsDataContext db = new studentsDataContext();

            if (!string.IsNullOrWhiteSpace(tel))
            {
                string newid = email.Substring(0, 3);
                user.firstconn = 0;
                user.Id = newid;
                user.name = newid;
                user.password = newid;
                user.picture = "defaultpp.png";
                
                if (sendMail(email, newid))
                {
                    db.students.InsertOnSubmit(user);
                    db.SubmitChanges();
                    ViewBag.ErrorMessage = "true";
                }
                else
                {
                    ViewBag.ErrorMessage = "false";
                }

                return View(new student());
            }
            else
            {
                if (user.grade == "Student")
                {
                    Boolean found = false;
                    string id = "abc";
                    var fetch = from x in db.students select new { id = x.Id, name = x.name, password = x.password };

                    foreach (var st in fetch)
                    {
                        if (st.name == user.name && st.password == user.password) {
                            found = true;
                            id = st.id;
                        }   
                    }
                    if (found)
                    {
                        return RedirectToAction("setId", "Students", new { id = id});
                    }
                    else
                    {
                        ViewBag.loginerror = "We are unable to find the entered user";
                        return View(new student());
                    }
                }
                else
                {
                    Boolean found = false;
                    var fetch = from x in db.teachers select new { name = x.name, password = x.password };

                    foreach (var te in fetch)
                    {
                        found = (te.name == user.name && te.password == user.password) ? true : found;
                    }
                    if (found)
                    {
                        return RedirectToAction("Index2", "Home", new { logon = user });
                    }
                    else
                    {
                        ViewBag.loginerror = "We are unable to find the entered user";
                        return View(new student());
                    }
                }  
            }
        }

        private bool sendMail(string email, string name)
        {
            try
            {
                string MailSender1 = "cmoxebo@gmail.com";
                string subject = "E-LEARNING subscription";
                string message = "Welcome to E-LEARNING. Here are your default login informations.\n\nYour username :  " + name + "\nYour password :  " + name + "\n\nRemember you have to change this information during your first login.\n\n\n\n\nDelivered by CURTIS MOXEBO";
                string MailPw1 = "2355AVMariette";

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(MailSender1, MailPw1);
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.EnableSsl = true;
                smtpClient.Timeout = 100000;

                MailMessage mailMessage = new MailMessage(MailSender1, email, subject, message);
                mailMessage.BodyEncoding = System.Text.UTF8Encoding.UTF8;

                smtpClient.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}