jQuery(document).ready(function($){
	var $form_modal = $('.cd-user-modal'),
		$form_login = $form_modal.find('#cd-login'),
		$form_signup = $form_modal.find('#cd-signup'),
		$form_forgot_password = $form_modal.find('#cd-reset-password'),
		$form_modal_tab = $('.cd-switcher'),
		$tab_login = $form_modal_tab.children('li').eq(0).children('a'),
		$tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
		$forgot_password_link = $form_login.find('.cd-form-bottom-message a'),
		$back_to_login_link = $form_forgot_password.find('.cd-form-bottom-message a'),
		$main_nav = $('.main-nav');
		$mycard = $('.card');

	$main_nav.on('click', function(event){
        if ($(event.target).is('.cd-signin') ) {
			$(this).children('ul').toggleClass('is-visible');
		} else {
			$main_nav.children('ul').removeClass('is-visible');
			$form_modal.addClass('is-visible');	
			( $(event.target).is('.cd-signup') ) ? signup_selected() : login_selected();
		}
	});
	
	$mycard.on('click', function(){
		$main_nav.children('ul').removeClass('is-visible');
			$form_modal.addClass('is-visible');	
			( $(event.target).is('.cd-signup') ) ? signup_selected() : login_selected();
	});

	$('.cd-user-modal').on('click', function(event){
		if( $(event.target).is($form_modal) || $(event.target).is('.cd-close-form') ) {
			$form_modal.removeClass('is-visible');
		}	
	});

	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$form_modal.removeClass('is-visible');
	    }
    });

	$form_modal_tab.on('click', function(event) {
		event.preventDefault();
		( $(event.target).is( $tab_login ) ) ? login_selected() : signup_selected();
	});

	$('.hide-password').on('click', function(){
		var $this= $(this),
			$password_field = $this.prev('span').prev('input');
		
		( 'password' == $password_field.attr('type') ) ? $password_field.attr('type', 'text') : $password_field.attr('type', 'password');
		( 'Hide' == $this.text() ) ? $this.text('Show') : $this.text('Hide');
		$password_field.putCursorAtEnd();
	});

	$back_to_login_link.on('click', function(event){
		event.preventDefault();
		login_selected();
	});

	function login_selected(){
		$form_login.addClass('is-selected');
		$form_signup.removeClass('is-selected');
		$form_forgot_password.removeClass('is-selected');
		$tab_login.addClass('selected');
		$tab_signup.removeClass('selected');
	}

	function signup_selected(){
		$form_login.removeClass('is-selected');
		$form_signup.addClass('is-selected');
		$form_forgot_password.removeClass('is-selected');
		$tab_login.removeClass('selected');
		$tab_signup.addClass('selected');
	}

    $form_login.find('input[type="submit"]').on('click', function (event) {
        var name = document.getElementById("signin-email").value;
        var password = document.getElementById("signin-password").value;
        var position = document.getElementById("position").value;

        if (!name) {
            $form_login.find('input[type="text"]').addClass('has-error').next('span').addClass('is-visible');
        }
        else {
            $form_login.find('input[type="text"]').removeClass('has-error').next('span').removeClass('is-visible');
        }
        if (position == "Select your position") {
            $form_login.find('select').addClass('has-error').next('span').addClass('is-visible');
        }
        else {
            $form_login.find('select').removeClass('has-error').next('span').removeClass('is-visible');
        }
        if (!password) {
            $form_login.find('input[type="password"]').addClass('has-error').next('span').addClass('is-visible');
        }
        else {
            $form_login.find('input[type="password"]').removeClass('has-error').next('span').removeClass('is-visible');
        }
        if (name && password && position != "Select your position") {
            $form_modal.removeClass('is-visible');
        }
        else {
            event.preventDefault();
        }
	});
	
	$form_signup.find('input[type="submit"]').on('click', function(event){
		var myemail = document.getElementById("signup-email").value;
        var mytel = document.getElementById("signup-username").value;
        var grade = document.getElementById("grade").value;

		var emailRegEx = /[a-zA-Z0-9]+(\.|_|-)?[a-zA-Z0-9]+@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)?\.\b(ca|com|fr|cm)\b/;
		var telRegEx = /^(\(\d{3}\)) ?\d{3}-\d{4}/;
		
        if (!emailRegEx.test(myemail)) { 
            $form_signup.find('input[type="email"]').addClass('has-error').next('span').addClass('is-visible');
		}
		else{
			$form_signup.find('input[type="email"]').removeClass('has-error').next('span').removeClass('is-visible');
        }
		if(!telRegEx.test(mytel)){
			$form_signup.find('input[type="text"]').addClass('has-error').next('span').addClass('is-visible');
        }
		else{
			$form_signup.find('input[type="text"]').removeClass('has-error').next('span').removeClass('is-visible');
        }
        if (grade == "Select your level of study") {
            $form_signup.find('select').addClass('has-error').next('span').addClass('is-visible');
        }
        else {
            $form_signup.find('select').removeClass('has-error').next('span').removeClass('is-visible');
        }
        if (emailRegEx.test(myemail) && telRegEx.test(mytel) && grade != "Select your level of study") {
            alert("Congratulation. You are now registered. Check your email to find your default user name and password");
            $form_modal.removeClass('is-visible');
        }
        else {
            event.preventDefault();
        }
	});

	/*if(!Modernizr.input.placeholder){
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
		  	}
		}).blur(function() {
		 	var input = $(this);
		  	if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.val(input.attr('placeholder'));
		  	}
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
		  	$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
			 		input.val('');
				}
		  	})
		});
	}*/
});

jQuery.fn.putCursorAtEnd = function() {
	return this.each(function() {
    	if (this.setSelectionRange) {
      		var len = $(this).val().length * 2;
      		this.setSelectionRange(len, len);
    	} else {
      		$(this).val($(this).val());
    	}
	});
};

jQuery('#cody-info ul li').eq(1).on('click', function(){
$('#cody-info').hide();
});